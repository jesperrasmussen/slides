var express = require('express'),
        app = express.createServer(),
        io = require('socket.io'),
        events = require('events');
var socket;
var tcpSocket;
var eventEmitter = new events.EventEmitter();
var conf;

exports.setup = function(configuration, endPoints, tcpSocket) {
    conf = configuration;
    
    app.configure(function () {
        app.use(express.methodOverride());
        app.use(express.bodyParser());
        app.use(app.router);

        app.use(express.static(__dirname + '/../Public'));
        app.use(express.errorHandler());
    });


    app.listen(configuration.webPort);

    socketio = io.listen(app);
    socketio.set('log level', 0);
    socketio.sockets.on('connection', function (client) {
        console.log('Web connection made. ');
        client.on('state', function (data) {
            console.log('Got command from web: ' + data.state + '. Pushing through UDP');
        	tcpSocket.sendMessage(data.state);
        });
    });
};