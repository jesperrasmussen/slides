exports.createOutgoingRequests = function (ip, port) {
    var outgoingService = function () {
        var self = this;
        var timeoutHandler;
        var socket;

        this.sendMessage = function (command) {
            var message = new Buffer(command);
            self.socket.send(message, 0, message.length, self.port, self.ip)
        };

        this.connectToServer = function (ip, port) {
            var dgram = require('dgram');
            
            self.ip = ip;
            self.port = port;
            self.socket = dgram.createSocket("udp4");
        };  

    };

    var udpservice;
    try {
        udpservice = new outgoingService();
        udpservice.connectToServer(ip, port);   
    } catch(ex) {
    }

    return udpservice;
};