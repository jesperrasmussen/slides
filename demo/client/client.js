var UDPClient = require('./servers/udpClient');
var conf = require('./configuration').conf;

var udp = UDPClient.createOutgoingRequests('localhost', conf.udpPort);

var webServer = require('./servers/webfrontClient');
webServer.setup(conf, [], udp);