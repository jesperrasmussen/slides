var configuration = {
    udpPort: 5692,
    webPort: 8081,
    useWebserver: true,
};

exports.conf = configuration;