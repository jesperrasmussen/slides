var UDPService = require('./servers/udpServer');
var conf = require('./configuration').conf;

var webServer = require('./servers/webfront');
webServer.setup(conf, []);

var udp = UDPService.createIncomingRequests(conf.udpPort);

UDPService.events.on('stateChange', function(data) {
    webServer.broadcast({state: data}, 'state');
});