var configuration = {
    udpPort: 5692,
    webPort: 8080,
    useWebserver: true,
};

exports.conf = configuration;