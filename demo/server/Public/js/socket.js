var socket = io.connect('http://'+window.location.hostname+':'+window.location.port);

socket.on('state', function (data) {
    if(data.state.indexOf('NEXT')>-1) {
        console.log('UDP Client received: ' + data.state + ' - Let\'s move on');
        demo.next();
    }
    
    if (data.state.indexOf('PREV')>-1) {
        console.log('UDP Client received: ' + data.state + ' - Let\'s go backwards');
        demo.prev();
    }
    
});