var express = require('express'),
        app = express.createServer(),
        io = require('socket.io'),
        events = require('events');
var socket;
var eventEmitter = new events.EventEmitter();
var conf;

exports.setup = function(configuration, endPoints) {
    conf = configuration;
    if(configuration.useWebserver===false)  return;
    endPoints = endPoints ||[];
    app.configure(function () {
        app.use(express.methodOverride());
        app.use(express.bodyParser());
        app.use(app.router);

        app.use(express.static(__dirname + '/../Public'));
        app.use(express.errorHandler());
    });

    for(var i=0;i<endPoints.length;i++) {
        app.get(endPoints[i].route, endPoints[i].handler);
    }

    app.listen(configuration.webPort);

    socket = io.listen(app);
    socket.set('log level', 0);
    socket.on('connection', function (client) {
        eventEmitter.emit('clientConnected', client);
        client.on('message', function (data) {
            eventEmitter.emit('clientMessage', data);
        });
        client.on('disconnect', function () {
            eventEmitter.emit('clientDisconnected');
        });
    });


};

exports.broadcast = function(message, channel) {
    if(conf.useWebserver === false)  return;
    channel = channel || 'message';
    socket.sockets.emit(channel, message);
};
exports.events = eventEmitter;
