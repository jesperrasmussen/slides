var events = require('events');
var eventEmitter = new events.EventEmitter();

exports.createIncomingRequests = function (port) {
    //Setup service.
    var incomingService = function() {
        var self = this;

        this.setupService = function(port) {
            var dgram = require('dgram');
            var server = dgram.createSocket('udp4');

            server.on("listening", function() {
                var address = server.address();
                console.log("server listening " +
                    address.address + ":" + address.port);

                self.address = address.address;
                self.port = address.port;
            });

            server.on("message", function(msg, rinfo) {
                self.handleMessage(msg.toString());
            });

            server.bind(port);
        };

        this.handleMessage = function(msg) {
            console.log('Got message ' + msg + '. Emitting to Slideshow service');
            if(msg.indexOf('NEXT')!=0 && msg.indexOf('PREV')!=0) return;
            eventEmitter.emit('stateChange', msg);
        };
    };

    var incoming = new incomingService();
    incoming.setupService(port);
    return incoming;
};

exports.events = eventEmitter;